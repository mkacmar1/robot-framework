
######################################################
#####      EXECUTION SPECIFIC CONFIGURATIONS     #####
######################################################
# BASE_URL: Points to the base url of the environment

#In addition to the variables, remember to set the environment variables needed. Refer to README.md for details.

--variable BASE_URL:https://www.google.com/
--variable API_BASE_URL:https://dummy.restapiexample.com
--variable SEARCH_EXPRESSION:TEST
--variable WEB_BROWSER:HeadlessChrome
#--variable WEB_BROWSER:Chrome

--variable SEARCH_FOR_TEXT:TEST
######################################################
#####            GENERAL CONFIGURATIONS          #####
######################################################
# The following variables are used in the test cases to configure
# include: includes the testcases with corresponding tag
# outputdir: used to specify location where the robot logs will be placed.
# ./xxx/xxx/xxx.robot -> change this to the file you want to execute tests from
#--include SIT, API, ALL -> test execution by tags is orchestrated by .yml file
--include ALL 
--outputdir ./outputs
#robot -A configfiles/qa.txt ./testCases/tests.robot

