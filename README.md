# Test Development Environment Setup
1. Install Python 3.8 + version
2. Add Python to PATH environment variables i.e.: C:\Python\Python38\Scripts\
3. Verify python and pip installed: python --version, pop --version
4. Download and install GITVisual 
5. Download and install your favorite editor i.e. Visual Studio Code
5. Download appopriate verision of chromedriver (https://chromedriver.storage.googleapis.com/) and save to folder i.e. C:\devtools\chromedriver
5. Add chromedriver to environment variables: 
6. Run pip list and check make sure virtualenv is installed, if not run pip install -U virtualenv
7. In your project create virtual environmnet using command: virtualenv venv
8. venv folder should be created
9. To activate virtual environment run: source ./venv/Scripts/activate or ./venv/Scripts/activate
10. Create .gitignore file and add venv/ there to make sure folder is excluded from commits
11. In root of the project create requirements.txt files with required liblaries
12. In activated venv run: pip install -r requirements.txt ,to install libraries from requirements

# Requirements.txt file example
* Make sure follwing items are included in requirments.txt file to support Robot Framework automation:
```
wheel==0.37.1
urllib3==1.26.9
oauthlib==3.2.0
requests==2.27.1
robotframework==5.0
robotframework-requests==0.9.2
robotframework-seleniumlibrary==6.0.0
selenium==4.1.3
```

# Known Errors
* SessionNotCreatedException: Message: session not created: This version of ChromeDriver only supports Chrome version 107 - download new version of chrome

# Run Tests Using Configfile
```
robot -A configfiles/qa.txt testCases/tests.robot
```

# Run robocop for static code analysis
* Documentation: https://robocop.readthedocs.io/en/stable/rules.html
```
robocop
```