*** Settings ***
Documentation     Test suite for all test cases.
Force Tags        ALL
Resource          ./keywords/web.resource
Resource          ./keywords/rest.resource
Resource          ./keywords/calculator.resource


*** Test Cases ***
WEB - Open Web Home Page and Search For Text
    [Documentation]  Open up the page and search for the phrase.
    ...              Verify search phrase returned by search engine.
    [Tags]  SIT
    Given I open web UI
    When I search for the phrase
    And I click Search
    Then I see the results for the phrase
    [Teardown]  Run Keywords
    ...   Close All Browsers

API - Open Web Home Page and Search For Text
    [Documentation]  Open session, request and verify employeed data.
    [Tags]   CI  API 
    Given Initialize Session
    When I Request Employees Data
    Then I Verify Employee Data
    [Teardown]  Run Keywords

CALC - Test Calculator
    [Documentation]  Open session, request and verify employeed data.
    [Tags]  CI  CALC
    Given The Calculator Is Running
    When The User Enters The Term "2 * 2"
    And The User Triggers The Calculation
    Then The Result Should Be "4"
